def test_index(app):
    res = app.get('/')
    assert res.status_code == 200
    assert res.content_type == 'text/html; charset=utf-8'
    assert 'Hello World' in res.data.decode('utf-8')
