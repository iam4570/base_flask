import pytest
import main


@pytest.fixture
def app():
    main.APP.testing = True
    test_app = main.APP.test_client()
    return test_app
