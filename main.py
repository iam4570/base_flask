from flask import (
    Flask,
    send_from_directory,
    render_template,
    # request,
    # redirect,
    # url_for,
)
APP = Flask(__name__)

@APP.route('/')
def index():
    return render_template('index.html', **{
        'hello': 'Hello World!'
    })

@APP.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@APP.route('/favicon.ico')
def send_image():
    return send_from_directory('images', 'favicon.png')


if __name__ == '__main__':
    APP.run(host='0.0.0.0', port='9006')
